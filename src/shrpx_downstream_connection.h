/*
 * Spdylay - SPDY Library
 *
 * Copyright (c) 2012 Tatsuhiro Tsujikawa
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef SHRPX_DOWNSTREAM_CONNECTION_H
#define SHRPX_DOWNSTREAM_CONNECTION_H

#include "shrpx.h"

#include "shrpx_io_control.h"
#include "shrpx_downstream.h"
#include <map>
#include <string>

namespace shrpx {

class ClientHandler;
class Downstream;

class PushedResponse {
public:
    PushedResponse(const int32_t& push_sid) : totalSizeReceived(0)
        , ds(NULL, 0, 0)
        , psid(push_sid)
        , real_ds(NULL)
    {}

    void append_data(const uint8_t *buf, const size_t len)
    {
        data.append((const char*)buf, len);
    }

    bool is_done_recv_from_server() const
    {
        return ds.get_response_state() == Downstream::MSG_COMPLETE;
    }

public:
	std::string data;
    int32_t totalSizeReceived; // of data, so far
    /* this is our fake downstream, which we used when there is no
     * real_ds yet */
    Downstream ds;
    /* the sid of the pushed stream */
    const int32_t psid;
    std::string pushedurl;

    Downstream *real_ds; /* shallow copy. after we find a match
                          * downstream, remember it here, so don't
                          * have to search again. if this is not NULL,
                          * then this pushedresponse has been claimed.
                          */
};

class DownstreamConnection {
public:
  DownstreamConnection(ClientHandler *client_handler);
  virtual ~DownstreamConnection();
  virtual int attach_downstream(Downstream *downstream) = 0;
  virtual void detach_downstream(Downstream *downstream) = 0;

  virtual int push_request_headers() = 0;
  virtual int push_upload_data_chunk(const uint8_t *data, size_t datalen) = 0;
  virtual int end_upload_data() = 0;

  virtual void pause_read(IOCtrlReason reason) = 0;
  virtual int resume_read(IOCtrlReason reason) = 0;
  virtual void force_resume_read() = 0;

  virtual bool get_output_buffer_full() = 0;

  virtual int on_read() = 0;
  virtual int on_write() = 0;

  virtual bool has_server_push() { return false; }
  virtual PushedResponse* claim_a_pushed_response(Downstream* downstream) { return NULL; }
  virtual void remove_pushed_response(PushedResponse *pr) {}

  ClientHandler* get_client_handler();
  void detach_client_handler(ClientHandler* ch);
  Downstream* get_downstream();
protected:
  ClientHandler *client_handler_;
  Downstream *downstream_;

public:
  const uint32_t instNum;

  static uint32_t nextInstNum;
};

} // namespace shrpx

#endif // SHRPX_DOWNSTREAM_CONNECTION_H
