/*
 * Spdylay - SPDY Library
 *
 * Copyright (c) 2012 Tatsuhiro Tsujikawa
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef SHRPX_SPDY_SESSION_H
#define SHRPX_SPDY_SESSION_H

#include "shrpx.h"

#include <set>
#include <map>
#include <vector>

#include <openssl/ssl.h>

#include <event.h>
#include <event2/bufferevent.h>

#include <spdylay/spdylay.h>

#include "http-parser/http_parser.h"

#include "shrpx_upstream.h"
#include "shrpx_downstream.h"
#include "shrpx_downstream_connection.h"


namespace shrpx {

class SpdyDownstreamConnection;

struct StreamData {
  SpdyDownstreamConnection *dconn;
};

class SpdySession {
public:
  SpdySession(event_base *evbase, SSL_CTX *ssl_ctx);
  ~SpdySession();

  int init_notification();

  int check_cert();

  int disconnect();
  int initiate_connection();

  void add_downstream_connection(SpdyDownstreamConnection *dconn);
  void remove_downstream_connection(SpdyDownstreamConnection *dconn);

  void remove_stream_data(StreamData *sd);

  int submit_request(SpdyDownstreamConnection *dconn,
                     uint8_t pri, const char **nv,
                     const spdylay_data_provider *data_prd);

  int submit_rst_stream(SpdyDownstreamConnection *dconn,
                        int32_t stream_id, uint32_t status_code);

  int submit_window_update(SpdyDownstreamConnection *dconn, int32_t amount);

  int32_t get_initial_window_size() const;

  bool get_flow_control() const;

  int resume_data(SpdyDownstreamConnection *dconn);

  int on_connect();

  int on_read();
  int on_write();
  int send();

  int on_read_proxy();
  int on_read_socks5_proxy();
  int on_write_socks5_proxy();

  int handle_server_push_ctrl_recv(spdylay_session* session,
		  	  	  	  	  	  	   spdylay_frame* frame);
  int handle_server_push_data_chunk_recv(spdylay_session* session,
                                         uint8_t flags,
                                         const int32_t sid,
                                         const uint8_t *data, size_t len);
  int handle_server_push_stream_close(spdylay_session* session,
                                         const int32_t sid,
                                         const spdylay_status_code status_code);
  bool is_pushed_stream(const int32_t& sid);
  Downstream *find_waiting_downstream_obj(const std::string& pushedurl);

  /* if there's a pushed response matching the request represented by
   * "downstream" argument, then link up the two and return the
   * pr. otherwise, return NULL.
   */
  PushedResponse* claim_a_pushed_response(Downstream *downstream);
  void remove_pushed_response(PushedResponse *pr);

  void clear_notify();
  void notify();

  bufferevent* get_bev() const;
  void unwrap_free_bev();

  int get_state() const;
  void set_state(int state);
  void set_socks5_state(int state);

  enum {
    // Disconnected
    DISCONNECTED,
    // Connecting proxy and making CONNECT request
    PROXY_CONNECTING,
    // Tunnel is established with proxy
    PROXY_CONNECTED,
    // Establishing tunnel is failed
    PROXY_FAILED,
    // Connecting to downstream and/or performing SSL/TLS handshake
    CONNECTING,
    // Connected to downstream
    CONNECTED
  };
  /* state of socks5 proxy */
  enum {
	  SOCKS5_NONE,
	  SOCKS5_GREETING,
	  SOCKS5_REQUESTING,
  };
private:
  event_base *evbase_;
  // NULL if no TLS is configured
  SSL_CTX *ssl_ctx_;
  SSL *ssl_;
  // fd_ is used for proxy connection and no TLS connection. For
  // direct or TLS connection, it may be -1 even after connection is
  // established. Use bufferevent_getfd(bev_) to get file descriptor
  // in these cases.
  int fd_;
  spdylay_session *session_;
  bufferevent *bev_;
  std::set<SpdyDownstreamConnection*> dconns_;
  std::set<StreamData*> streams_;
  int state_;
  int socks5_state_;
  bool notified_;
  bufferevent *wrbev_;
  bufferevent *rdbev_;
  bool flow_control_;
  // Used to parse the response from HTTP proxy
  http_parser *proxy_htp_;

public:
  /* this is for saving partial (in-progress) or fully received pushed
   * objects. as soon as a matching request from client comes in, any
   * partially or fully received data should be delivered to the
   * client, and the key-value removed from the map, especially
   * necessary if the object has only been partially received, because
   * the next pushed data chunk comes in for this object, will be
   * immediately delivered to the client without being saved in this
   * map.
   *
   * the key is the full object url (e.g. http://www....)
   */
  std::map<int32_t, PushedResponse* > psid2pr_;
  /* map from an sid to list of (push) sids associated with it */
  std::map<int32_t, std::vector<int32_t > > sid2psids_;
  int numClaimed_;

  /* monotonically increasing instance number */
  const uint32_t instNum;

  static uint32_t nextInstNum;
};

} // namespace shrpx

#endif // SHRPX_SPDY_SESSION_H
