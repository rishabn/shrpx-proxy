/*
 * Spdylay - SPDY Library
 *
 * Copyright (c) 2012 Tatsuhiro Tsujikawa
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include "shrpx_downstream_connection.h"

#include "shrpx_client_handler.h"
#include "shrpx_downstream.h"

namespace shrpx {

uint32_t DownstreamConnection::nextInstNum = 0;

DownstreamConnection::DownstreamConnection(ClientHandler *client_handler)
  : client_handler_(client_handler),
    downstream_(0)
  , instNum(nextInstNum)
{
    ++nextInstNum;
}

DownstreamConnection::~DownstreamConnection()
{}

ClientHandler* DownstreamConnection::get_client_handler()
{
  return client_handler_;
}

void DownstreamConnection::detach_client_handler(ClientHandler *ch)
{
    if (ch != client_handler_) {
        DCLOG(FATAL, this) << "we are not attached to ClientHandler "
                           << ch << " --> exit!";
        // exit so we don't "fail silently"
        exit(1);
    }
    client_handler_ = NULL;
}

Downstream* DownstreamConnection::get_downstream()
{
  return downstream_;
}

} // namespace shrpx
