until ./shrpx --client --frontend=130.245.150.135,3000 --backend=130.126.142.171,80 --backend-spdy-proto=spdy/3 --cacert=../certificates/cacert.pem --insecure --log-level=INFO  --add-x-pnp; do
    echo "SHRPX Proxy quit with exit code $?.  Respawning.." >&2
    sleep 1
done
